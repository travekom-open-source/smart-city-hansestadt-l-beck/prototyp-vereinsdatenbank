# Prototyp Vereinsdatenbank

Zur Verwaltung von Vereinen in der Hansestadt Lübeck soll eine Vereinsdatenbank geschaffen werden. Vorgaben waren hier keine vorhanden, so dass mit diesen Prototypen eine erste Diskussionsgrundlage geschaffen wurde, um die Anforderungen im Rahmen eines Umsetzungsprojektes weiter zu schärfen.

Die Vereinsdatenbank besteht aus insgesamt fünf einzelnen Seiten im Corporate Design der Hansestadt Lübeck:

-	Verwaltungsfrontend
-	Upload von bestehenden Vereinsdaten per CSV
-	Login
-	Öffentliche Seite zum Suchen der Vereine
-	Öffentliche Detailseite eines Vereins

Im Verwaltungsfrontend können Vereine angelegt und verwaltet werden. Über den Button „Neuen Eintrag anlegen“ bzw. durch einen Klick auf einen Verein in der Liste öffnet sich eine Sidebar mit den Details zu einem Verein. Mittels der Upload Funktion können aktualisierte Listen von Vereinen als CSV hochgeladen werden. Das Verwaltungsfrontend ist über eine Loginfunktion nutzbar, auf welches nur autorisierte Nutzer Zugriff haben. In der öffentlich zugänglichen Suchmaske werden die Vereine aufgelistet bzw. sind per Karte aufrufbar. Die Karte und Liste können nach verschiedenen Faktoren gefiltert werden. Diese sind:

-	Name
-	Kategorie (Musik, Sport, Sonstige)
-	Stadtteil

Mit einem Klick auf den Titel eines Vereins gelangen die Nutzer:innen zu der Detailseite des Vereines. Hier sind nicht alle Elemente aufgeführt, sondern nur die, welche zur öffentlichen Nutzung freigegeben wurden.

Zur Persistenz der Daten wird im Prototyp die integrierte Datenbank der Application Builder Platform verwendet. Bei eine Produktivstellung muss hier eine entsprechende Robuste Datenbanktechnologie verwendet werden.

<img src="https://gitlab.com/travekom-open-source/smart-city-hansestadt-l-beck/prototyp-vereinsdatenbank/-/raw/main/Prototyp_Vereinsdatenbank_Maßnahme_6_9.png">

## Installation
Zur Installation der App kann Budibase in der Cloud- oder onPremise Variante genutzt werden. Beim Erstellen einer neuen App kann dann das hier hinterlegte Archiv importiert werden.

## Nutzung
Die App ist nicht für eine produktive Nutzung gedacht, sondern wurde im Rahmen einen Domain Prototypings erstellt um als Grundlage für weitere Projekte zu dienen. Das per iFrame eingebundene Widget von wheelmap.org muss bei produktiver Nutzung gegen eine jährliche Gebühr beim Anbieter freigeschaltet werden.

## Support
Anfragen zum Projekt an team_trk_udp@travekom.de

## Contributing
Veränderungen am Sourcecode können in Rahmen von Feature-Branches mit anschließendem MergeRequest vorgenommen werden. Der MergeReqeust wird von einem Maintainer der reviewed.

## License
Das Projekt steht unter der Apache 2.0 Lizenz
